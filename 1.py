from functools import reduce
MAX = 1000

def mul(n):
    return {x*n for x in range(1, int((MAX+n-1)/n))}

mul3 = mul(3)
mul5 = mul(5)
mul35 = mul3.union(mul5)
print(reduce(lambda x, y: x+y, list(mul35)))
