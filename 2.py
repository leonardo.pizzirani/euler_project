from functools import reduce, lru_cache

MAX = 4.0e6

@lru_cache
def fibo(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo(n-2) + fibo(n-1) 

def fibos():
    i = 1
    f = 0
    while(f < MAX):
        f = fibo(i)
        i += 1
        yield f

print(reduce(lambda x, y: x+y, filter(lambda x: x%2==0, fibos())))
